# Crossplane

## Requirements
 - *GKE*
 - *Google Cloud SDK (316.+)*
 - *bq (2.0.62+)*
 - *core (2020.10.23.+)*
 - *gsutil (4.54.+)*
 - *Kubernetes(1.16.+)*
 - *kubectl(1.16.+)*
 - *Helm(3.+)*
 - *ansible (2.10.+)*
 - *openshift python client (0.11.+)*
 - *PyYAML (5.3.+)*

## Deploy

  1. Git clone this repo to bastion

  2. Check if roles/crossplane/defaults/main.yml is right for the environment you want to deploy

  | Input variables in roles/crossplane/defaults/main.yml | Comments |
  | ------------- |:-------------:|
  | gcp_project_id: 'opg-dev-temp'     | A customizable unique identifier for your project |
  | gke_name: 'poc-crossplane'         | Name of your Google Kubernetes Engine (GKE) |
  | gke_zone: 'us-east1-d'             | GKE installed zone |
  | namespace: 'crossplane-system'     | Namespace where crossplane will be deployed (will be created if not exist) |
  | new_sa_name: 'account-name'        | GKE Service Account Name |
  | service: 'sqladmin.googleapis.com' | Service will be enable to configure cloudSQL provider |
  | role: 'roles/cloudsql.admin'       | GCP Service Account Role |

  3. Deploy with ansible command:
```
  - $ ansible-playbook -i ./hosts -e 'ansible_python_interpreter=/usr/bin/python3' crossplane.yml -vvv
```
  *After the Deploy you must take care about the file creds.json. It will be located at the repo root, and has the service account credentials.*

  4. Check Crossplane Installation
```
  - $ kubectl get all -n crossplane-system
  - $ kubectl crossplane --help
```
